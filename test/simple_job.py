import job_farmer
import logging

# To be able to see the log messages from the farmer
logging.basicConfig(level=logging.INFO)

# Select the system you want to run on
system = job_farmer.CoriHaswell()

# Create the farmer object
farmer = job_farmer.Farmer(system)

# Create the JobConfiguration
job = job_farmer.JobConfiguration(name='test_job',
                                  account='account',
                                  queue='debug',
                                  duration='00:04:00',
                                  max_nodes_per_job=2,
                                  pre_container_env_setup_cmds=['module load python', 'conda activate job_farmer']
                                  )

# Add the job configuration to the farmer
farmer.add_global_job_configuration(job)

# Add 30 new tasks to the farmer
for i in range(30):
    task = job_farmer.Task(commands=['echo "${MY_VAR}"',
                                     'sleep 1',
                                     'echo After sleep: ${MY_VAR}'],
                           env={'MY_VAR': f'Hello World {i}'},
                           n_threads=1)
    farmer.add_task(job.name, task)

# Build the task list and the slurm configuration
farmer.build()

# Do a dry run of the submission
farmer.submit(dry_run=True)
