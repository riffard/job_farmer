from job_farmer import System


def test_system():
    input_system_name = 'test'
    input_physical_cpu_cores_per_node = 101
    input_threads_per_cpu_core = 202
    input_physical_gpu_per_node = 303
    input_memory_per_node_gb = 404
    input_queues = ['Queue_1', 'Queue_2']

    system = System(
        system_name=input_system_name,
        physical_cpu_cores_per_node=input_physical_cpu_cores_per_node,
        threads_per_cpu_core=input_threads_per_cpu_core,
        physical_gpu_per_node=input_physical_gpu_per_node,
        memory_per_node_gb=input_memory_per_node_gb,
        queues=input_queues
    )

    assert system.name == input_system_name
    assert system.physical_cpu_cores_per_node == input_physical_cpu_cores_per_node
    assert system.threads_per_cpu_core == input_threads_per_cpu_core
    assert system.memory_per_node_gb == input_memory_per_node_gb
    assert system.queues == input_queues
    assert system.threads_per_node == input_physical_cpu_cores_per_node * input_threads_per_cpu_core
    assert system.__str__() != ''
    assert system.get_constrains() == []

